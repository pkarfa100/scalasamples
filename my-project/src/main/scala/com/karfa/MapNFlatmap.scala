package com.karfa

object MapNFlatmap extends App {

  //map
  //Evaluates a function over each element in the list, returning a list with the same number of elements.
  val numbers = List(1, 2, 3, 4)

  println("X 2")
  val mappedX2 = numbers.map((i: Int) => i * 2)
  mappedX2.foreach(println)

  println("X 3")
  val mappedX3 = numbers.map(timesThree)
  mappedX3.foreach(println)

  def timesThree(i: Int): Int = i * 3

  //flatten
  //flatten collapses one level of nested structure.
  println("Flatten")
  val flattened = List(List(1, 2), List(3, 4)).flatten
  flattened.foreach(println)

  /** flatMap
    * flatMap is a frequently used combinator that combines mapping and flattening.
    * flatMap takes a function that works on the nested lists and then concatenates the results back together.
    */
  println("Flat Map")
  val nestedNumbers = List(List(1, 2), List(3, 4))
  nestedNumbers.flatMap(x => x.map(_ * 2)).foreach(println)

  println("Flat Map as a Map+Flatten")
  //Think of it as short-hand for mapping and then flattening:
  nestedNumbers.map((x: List[Int]) => x.map(_ * 2)).flatten.foreach(println)


  val morenumbers = List(1, 2, 3, 4)

  // morenumbers.flatMap(x=> x.map(_ * 2))
}

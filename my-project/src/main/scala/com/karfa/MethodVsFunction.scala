package com.karfa

object MethodVsFunction extends App {

  val obj = new cls1

  println(obj.functionExmpl("Jack"))

  println(obj.methodExmpl("Jill"))

  println(obj.methodExmpl("Monty"))

}

class cls1 {

  val functionExmpl: String => Int = { i: String =>
    println("hello world from function :" + i)
    5 * 2
  }

  def methodExmpl(x: String): Int = {
    println("hello world from Method :" + x)
    10 * 2
  }

  def methodExmpl2(x: String) = {
    println("hello world from Method :" + x)
    15 * 2
  }

}

/**
// protected val mergeStore: Store => OptionT[Future, Store] = { store: Store =>
  mergeWithDb(store)(loadStoreFromDb, app.storeRepo.insert _, app.storeRepo.update _)
  }

  */

package com.karfa

object ImplicitParam extends App {

  /** The second parameter is declared implicit
    * This means we can explicitly pass that argument if we wish. But if we omit it, the Scala compiler will look
    * for the missing parameter in the current scope, hunting for a value declared with the keyword “implicit” and with the expected type (Int)
    *
    * There are two key things to note here. The first is that once a parameter is marked as implicit, a matching
    * implicit value only needs to be brought into scope once, rather than at passed in at every call site.
    * One can immediately see the potential for reducing boilerplate but also for obfuscating the actual behaviour of a function.
    * *
    * The other thing to note is that the compiler matches up an implicit parameter with its implicit value via the type,
    * and it will only supply an implicit value if it finds exactly one in scope that matches.
    * This is why the final line gives a compiler error: by this point we’ve declared two implicit values of type Int,
    * both of which could be used to fill in the missing parameter.
    */
  def multiply(x: Int)(implicit y: Int) = x * y

  multiply(3)(10) // 30
  multiply(4)(10) // 40

  multiply(3)
  //comment the implicit val in next line and see that this  code would not compile
  // error: could not find implicit value for parameter factor: Int

  implicit val z: Int = 10

  multiply(3) // 30
  multiply(4) // 40

  /**
    *  Uncomment the next line and it would
  // error: ambiguous implicit values:
  // both value y of type => Int
  // and value z of type => Int
    */
  //implicit val z2: Int = 11
  multiply(3)

}

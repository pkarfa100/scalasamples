package com.karfa

object TypeClasses extends App {


  /** ******************************************************  FIRST  PASS ******************************************************
    * The idea of typeclasses is that you provide evidence that a class satisfies an interface.
    * The idea of typeclasses is that you provide evidence (WrapperCanFoo) that
    * a class (Wrapper) satisfies an interface (CanFoo).
    * Instead of having Wrapper implement an interface directly,
    * typeclasses let us split up the definition of the class and
    * the implementation of the interface.
    * That means I can implement an interface for your class, or that a third party
    * can implement my interface for your class, and everything basically ends up working out.
    * Trying to solve this modularity problem without typeclasses leads to ugly subtyping hacks.
    */
  trait CanFoo1[A] {
    def foos(x: A): String
  }

  case class Wrapper1(wrapped: String)

  object WrapperCanFoo1 extends CanFoo1[Wrapper1] {
    def foos(x: Wrapper1) = {
      println("I am going to foo " + x.wrapped)
      x.wrapped
    }
  }
  /**
    * But there's a clear problem. If you want to take a thing that CanFoo,
    * you need to both ask for the class instance and the proof from your caller.
    */
  def foo1[A](thing: A, evidence: CanFoo1[A]) = evidence.foos(thing)

  // These evidence objects are fairly onerous to keep passing around.
  foo1(Wrapper1("Tom"), WrapperCanFoo1)

  /** ******************************************************SECOND PASS ***************************************************
    * //  couldn't we solve this with implicits? Let's try that
    */
  implicit object WrapperCanFoo2 extends CanFoo1[Wrapper1] {
    def foos(x: Wrapper1) = {
      println("I am going to foo " + x.wrapped)
      x.wrapped
    }
  }

  def foo2[A](thing: A)(implicit evidence: CanFoo1[A]) = evidence.foos(thing)

  /**
    * The evidence object is the only thing in scope of type CanFoo[Wrapper],
    * and so it gets passed as the implicit evidence argument.
    */
  foo2(Wrapper1("Dick"))

  /** ****************************************************** THIRD PASS *************************************************
    * // Turns out, Scala has syntax sugar for this particular usecase.
    */


  def foo3[A: CanFoo1](thing: A) = implicitly[CanFoo1[A]].foos(thing)

  foo3(Wrapper1("Harry"))

  /** ******************************************************FOURTH  PASS ***************************************************
    * we can make that look even nicer with one particular bit of additional syntax sugar, such that the following compiles.
    * Here, foo4 calls CanFoo.apply to retrieve the evidence argument.
    * Note how since the implicit parameter is in scope inside foo, it gets implicitly passed to CanFoo.apply.
    */

  trait CanFoo2[A] {
    def foos(x: A): String
  }

  object CanFoo2 {
    def apply[A:CanFoo2]: CanFoo2[A] = implicitly
  }

  case class Wrapper2(wrapped: String)

  implicit object WrapperCanFoo extends CanFoo2[Wrapper2] {
    def foos(x: Wrapper2) = {
      println("I am going to foo gracefully " + x.wrapped)
      x.wrapped
    }
  }

  def foo4[A:CanFoo2](thing: A) = CanFoo2[A].foos(thing)  //Note the change

  foo4(Wrapper2("Jesus"))
  //foo4(Wrapper1("Alla")) //Why this doesn't work !!

  /** ******************************************************FIFTH  PASS ***************************************************
   Alternatively or additionally, we can use an implicit conversion to make the methods look like they're available on the object.
    Here, Scala applies the implicit conversion to CanFooOps at call-site, so we effectively have CanFooOps(thing).foo.
    */

  implicit class CanFooOps[A:CanFoo2](thing: A) {
    def foo = CanFoo2[A].foos(thing)
  }

  //def foo[A:CanFoo2](thing: A) = thing.foo

  Wrapper2("Bhagoban").foo
}

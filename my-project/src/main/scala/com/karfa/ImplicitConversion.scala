package com.karfa

import java.net.URL

import scala.io.Source

object ImplicitConversion extends App {

  //https://www.theguardian.com/info/developer-blog/2016/dec/22/parental-advisory-implicit-content
  /** Example - 1
    * Scala compiler can do implicitly is transform one type into another. It will look for opportunities
    * to do this whenever there is a type mismatch, i.e. when the compiler expects an expression to be of
    * one type but it is actually of a different type. Here’s another contrived example
    * *
    * Since alert expects a String, passing an Int would normally give a compiler error.
    * But if we have in scope an implicit def which takes in an Int and spits out a String,
    * then the compiler will pass the value with the mismatched type through this function.
    */
  def alert(msg: String): Unit = println(msg)
  alert(7)
  /** comment the following line and the alert method will give you an compilation error */
  implicit def intToString(i: Int): String = i.toString





  /** Example - 2
    * The compiler will also look for opportunities to implicitly convert types when code tries to
    * access an object member which is not defined for that type. Consider the following:
    *
    * @param x
    */
  class LoquaciousInt(x: Int) {
    def chat: Unit = for (i <- 1 to x) println("Hi!")
  }
  implicit def intToLoquaciousInt(x: Int) = new LoquaciousInt(x)
  //Comment the above implicit definition and you would see that the following line would not compile
  3.chat


  /** Example - 3
    * The example - 2 above, where we are converting to a new type which we have defined ourselves,
    * is the most common use case for the more general process of implicit conversion.
    *It is common enough that it has its own shorthand: an implicit class.
    * If you have a class whose constructor takes a single argument,
    * as above, then it can be marked as implicit and the compiler will automatically allow
    * implicit conversions from the type of its constructor argument to the type of the class.
    * So the above definition of a class and implicit def can be rewritten as follows:
    *
    *
    * This process is often referred to as type enrichment and can be used whenever
    * we want to add extra functionality to classes which may be defined in libraries
    * whose source code we cannot modify.
    */
  implicit class LoquaciousIntX(x: Int) {
    def chatX: Unit = for(i <- 1 to x) println("Hello!")
  }
  3.chatX

  //Another use of example- 3
  implicit class Agent(code: Int) {
    def codename = s"00$code"
  }
  def hello(agent: Agent) = {
    val x = s"Hello, ${agent.codename}!"
    println(x)
  }
  hello(7)
  /***
    *
    *
    * We have on the one hand the ability of implicit parameters to magically supply
    * missing arguments of a matching type. We have on the other hand the ability of
    * implicit conversions to magically turn an object from one type into another
    * in order to supply missing functionality.
    * The combination of these things proves particularly useful in Scala when defining
    * and using type classes.
    *
    */

  ///read this article http://eed3si9n.com/implicit-parameter-precedence-again
}

package com.karfa

object Objects extends App {

  val obj = Timer

  obj.currentCount()

  obj.currentCount()

  obj.currentCount()

}

object Timer {
  var count = 0

  def apply(foo: String) = new Timer(foo)

  def currentCount(): Long = {
    count += 1
    println(count)
    count
  }
}

class Timer(foo: String)

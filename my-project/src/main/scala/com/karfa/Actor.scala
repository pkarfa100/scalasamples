package com.karfa

import akka.actor.Actor
import akka.actor.ActorSystem
import akka.actor.Props
import scala.concurrent.duration._

object Actor extends App {

  val system = ActorSystem("HelloSystem")
  // default Actor constructor
  val helloActor = system.actorOf(Props[HelloActor], name = "helloactor")
  helloActor ! "hello"
  helloActor ! "buenos dias"

  val Tick = "tick"


  //Use system's dispatcher as ExecutionContext
  import system.dispatcher

  //This will schedule to send the Tick-message
  //to the tickActor after 0ms repeating every 50ms
  val cancellable =
  system.scheduler.schedule(
    0 milliseconds,
    1 milliseconds,
    helloActor,
    Tick)

  //This cancels further Ticks to be sent
  //cancellable.cancel()
}

class HelloActor extends Actor {
  def receive = {
    case "hello" => println("hello back at you")
    case "tick" => println("Tick Tock")
    case _ => println("huh?")
  }
}

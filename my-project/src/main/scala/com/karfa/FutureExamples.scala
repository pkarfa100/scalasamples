package com.karfa

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Random
import scala.util.{Failure, Success}

object FutureExamples extends App {

  //FunFuturewithBlock(1)
  //FunfutureWithCallback(2)
  FunFutureWithOnSuccessAndFailure(3)


  def FunFuturewithBlock(x: Int): Unit = {

    // used by 'time' method
    implicit val baseTime = System.currentTimeMillis

    /**
      * A Future is created after the second comment. Creating a Future is simple; you just pass it a block
      * of code you want to run. This is the code that will be executed at some point in the future.
      */
    // 2 - create a Future
    val f = Future {
      sleep(500)
      1 + 1
    }

    /**
      * The Await.result method call declares that it will wait for up to one second for the Future to return.
      * If the Future doesn’t return within that time, it throws a java.util.concurrent.TimeoutException. */
    // 3 - this is blocking (blocking is bad)
    val result = Await.result(f, 1 second)
    println(result)

    /**
      * The sleep statement at the end of the code is used so the program will keep running while the
      * Future is off being calculated. You won’t need this in real-world programs, but in small example programs like this,
      * you have to keep the JVM running.
      */
    sleep(1000)

  }

  def FunfutureWithCallback(x: Int): Unit = {

    println("starting calculation ...")
    val f = Future {
      sleep(Random.nextInt(500))
      42
    }
    println("before onComplete")

    /**
      *
      * The f.onComplete method call sets up the callback. Whenever the Future completes,
      * it makes a callback to onComplete, at which time that code will be executed.
      */
    f.onComplete {
      case Success(value) => println(s"Got the callback, meaning = $value")
      case Failure(e) => e.printStackTrace
    }
    // do the rest of your work
    println("A ...");
    sleep(100)
    println("B ...");
    sleep(100)
    println("C ...");
    sleep(100)
    println("D ...");
    sleep(100)
    println("E ...");
    sleep(100)
    println("F ...");
    sleep(100)
    sleep(2000)
  }

  def FunFutureWithOnSuccessAndFailure(x: Int): Unit = {

    val f = Future {
      sleep(Random.nextInt(500))
      if (Random.nextInt(500) > 250) throw new Exception("Yikes!") else 42
    }

    /**
      * onSuccess and onFailure blocks are defined as partial functions; they only need to handle their expected conditions.
      */
    f onSuccess {
      case result => println(s"Success: $result")
    }
    f onFailure {
      case t => println(s"Exception: ${t.getMessage}")
    }

    // do the rest of your work
    println("A ...");
    sleep(100)
    println("B ...");
    sleep(100)
    println("C ...");
    sleep(100)
    println("D ...");
    sleep(100)
    println("E ...");
    sleep(100)
    println("F ...");
    sleep(100)
    sleep(2000)
  }

  def sleep(time: Long) {
    Thread.sleep(time)
  }

}

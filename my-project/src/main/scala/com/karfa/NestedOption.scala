package com.karfa

object NestedOption extends App {


  /**
    * a common use of flatMap. Depending on your needs, you can use a similar approach with any monad, i.e., Try, Either, List, Future, etc.
    * *
    * Here’s a quick explanation of the getStreet2 code:
    * *
    * maybePerson is an Option, so you flatMap it to get a Person instance
    * Because address in Person is also an Option, you flatMap it to get an Address instance
    * Once you have the address, you yield address.street2
    */
  def getStreet2(maybePerson: Option[Person]): Option[String] = {
    maybePerson flatMap { person =>
      person.address flatMap { address =>
        address.street2
      }
    }
  }

  /** most people prefer for expressions to flatMap, so I suspect that they’d write getStreet2 like this: */
  def getStreet3(maybePerson: Option[Person]): Option[String] = {
    for {
      person <- maybePerson
      address <- person.address
      street2 <- address.street2
    } yield street2
  }

  /**
    * A second reason for writing this article was to give you a little more exposure to flatMap.
    * When I first learned Scala, I always thought of flatMap in terms of collections classes, but once you get into other monadic data types like Option, Try, Either, etc.,
    * you realize that there’s a very different way to think about flatMap, as shown in this example.
    */

  println("Case for none")
  val a1 = Address("123 Main Street", None, "Talkeetna", "Alaska", "99676")
  val p1 = Person("Al", Some(a1))
  println(getStreet2(Some(p1)))
  println(getStreet3(Some(p1)))


  println("Case for some")
  val a2 = Address("123 Main Street", Some("Apt. 1"), "Talkeetna", "Alaska", "99676")
  val p2 = Person("Al", Some(a2))
  println(getStreet2(Some(p2)))
  println(getStreet3(Some(p2)))


}

case class Address(
                    street1: String,
                    street2: Option[String],
                    city: String,
                    state: String,
                    zip: String
                  )


case class Person(
                   name: String,
                   address: Option[Address]
                 )
